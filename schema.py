import graphene
from django.db.models import Count
from graphene_django import DjangoObjectType

from recipes.models import Recipe, Ingredient


class RecipeType(DjangoObjectType):
    id = graphene.Int(source='pk')

    class Meta:
        model = Recipe
        fields = ("id", "title", "description", "ingredients")


class IngredientType(DjangoObjectType):
    id = graphene.Int(source='pk')

    class Meta:
        model = Ingredient
        fields = ("id", "name", "recipe")


class Query(graphene.ObjectType):
    recipe_by_id = graphene.Field(RecipeType, recipe_id=graphene.Int(required=True))
    search_by_ingredient = graphene.List(RecipeType, name=graphene.String(required=True))
    search_by_ingredient_count = graphene.List(RecipeType, count=graphene.Int(required=True))

    def resolve_recipe_by_id(root, info, recipe_id):
        try:
            return Recipe.objects.get(id=recipe_id)
        except Recipe.DoesNotExist:
            return None

    def resolve_search_by_ingredient(root, info, name):
        q1 = Recipe.objects.filter(title__contains=name)
        q2 = Recipe.objects.filter(ingredients__name__contains=name)
        return q2.union(q1).all()

    def resolve_search_by_ingredient_count(root, info, count):
        return Recipe.objects.annotate(ingredients_count=Count('ingredients'))\
            .filter(ingredients_count__lt=count).prefetch_related('ingredients').all()


schema = graphene.Schema(query=Query)
