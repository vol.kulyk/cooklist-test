This exercise is to implement the best possible solution to the exercise below in ~3 hours. You do not have to complete the entire assignment, just make as much progress as you can within this timeframe.

We're evaluating your ability to take a set of requirements and spike a holistic solution that demonstrates craftsmanship, thoughtfulness and good architectural design. We’re looking for something that is beautiful, intuitive and easy to debug/test/extend 😃

Ideally your solution would have a readme that explains how to run it locally and test the results so we can fully analyze your efforts.


### Project Overview
1. Our Recipe AI has generated a list of 100 Creative AI Recipes and we want to make these available through a GraphQL API.

2. Create a simple Django project that stores the recipes and their related ingredients and directions that are found in the file in a database and then makes them available through a GraphQL API.
graphene-django is recommended but not required

3. You’ll need to define your models, write a function to import the data and store it, and then define the schema to make the data available through a /graphql endpoint. 

4. Set up a test which returns the data for a single recipe by its ID

5. Set up a test which returns recipes that have “bacon” in the title or ingredient list

6. Set up a test which returns recipes that have < 5 ingredients

7. Include a Readme file explaining the steps to run the project


Bonus
Avoid the N+1 problem when returning objects from related models


## Run
Developed using Python version 3.10

install requirements
```
pip install -r requirements.txt
```
run the app
```
python manage.py runserver
```

run tests
```
python manage.py test
```

## Comments

This is a quick solution which has a wide range for improvement. 
I will describe here my implementation and possible improvements. 

1. Our Recipe AI has generated a list of 100 Creative AI Recipes and we want to make these available through a GraphQL API.
2. ...
3. You’ll need to define your models, write a function to import the data and store it, and then define the schema to make the data available through a /graphql endpoint. 

The main problem here is that a provided data is not structured well. 
An item of ingredient list consists of amount, unit, and ingredient.
Very often ingredient has detailed description. For example `butternut squash, peeled, seeded, and cubed`. 
Also there are cases where two or more ingredients are in the same item of list: `Salt and pepper, to taste`.
Sometimes there is a choice what to use: `4 cups chicken or vegetable stock`. 

Let suppose we have a well-structured ingredients description. 
So we could create a model `PureIngredient` that stores all these ingredients, a model `Recipe` for recipe itself.
Then we could create a model `RecipePureIngredient` for creating many-to-many relation and storing amount and units.
Such solution would provide a good structure for implementing it using a relational database. 
Using index we could achieve a good performance for searching recipes by ingredients.

According to all these parsing problems mentioned above, it is hard to extract ingredients properly in given timeframe.
So I realized that I will work with ingredient's names that can be 'dirty' as my cleaning rules can omit something. 
For example, name can be `pesto (homemade or store-bought)` instead of `pesto`. 
So for searching ingredients I chose full-text search on the field `name` and removed many-no-many relation.
For the best practice ElasticSearch will do it the best.
It also can be implemented using GIN index in Postgres. 
To make project easy to run I used SQLite without optimizations for full-text search.

Script extract.py parses recipes and ingredients

