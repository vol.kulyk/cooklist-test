import factory
from factory.django import DjangoModelFactory

from . import models


class RecipeFactory(DjangoModelFactory):
    class Meta:
        model = models.Recipe

    title = factory.Sequence(lambda n: f"Recipe Title {n}")
    description = factory.Sequence(lambda n: f"Recipe Description {n}")


class IngredientFactory(DjangoModelFactory):
    class Meta:
        model = models.Ingredient

    name = factory.Iterator(['milk', 'bread', 'cacao', 'sugar', 'nut'])


