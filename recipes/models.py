from django.db import models


class Recipe(models.Model):
    title = models.CharField(max_length=100)
    description = models.TextField()

    def __str__(self):
        return self.title


class Ingredient(models.Model):

    name = models.CharField(max_length=100)
    recipe = models.ForeignKey(
        Recipe, related_name="ingredients", on_delete=models.CASCADE
    )
    # amount = models.FloatField()  # example of model improvement
    # unit = models.CharField(
    #     max_length=25,
    #     choices=(
    #         ("unit", "units"),
    #         ("tbsp", "tablespoon"),
    #         ("tsp", "teaspoon"),
    #         ("cup", "cup"),
    #         ("oz", "ounce"),
    #         ("lb", "pound"),
    #     ),
    # )

    def __str__(self):
        return self.name
