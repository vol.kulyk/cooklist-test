import json

from graphene_django.utils import GraphQLTestCase

from recipes.models import Recipe, Ingredient
from recipes.utils import RecipeFactory, IngredientFactory


class RecipeGetByIdTests(GraphQLTestCase):

    def setUp(self) -> None:
        self.query_string = '''
            query recipeById($id:Int!){
              recipeById(recipeId:$id){
                id
                title
              }
            }
            '''

    def tearDown(self) -> None:
        Recipe.objects.all().delete()

    def test_get_by_id__success(self):
        recipe = RecipeFactory()
        response = self.query(self.query_string, variables={'id': recipe.id})
        self.assertResponseNoErrors(response)
        content = json.loads(response.content)
        self.assertEqual(content['data']['recipeById']['id'], recipe.id)

    def test_get_by_id__no_found(self):
        response = self.query(self.query_string, variables={'id': 123})
        self.assertResponseNoErrors(response)
        content = json.loads(response.content)
        self.assertIsNone(content['data']['recipeById'])


class RecipeGetByIngredientName(GraphQLTestCase):

    def setUp(self):
        self.query_string = '''
            query searchByIngredient($name:String!){
              searchByIngredient(name:$name){
                title,
                ingredients {
                  name
                }
              }
            }
            '''
        self.recipes = RecipeFactory.create_batch(6)
        ingredient_list = ['bacon', 'bread', 'cacao', 'sugar', 'nut']
        for i, recipe in enumerate(self.recipes):
            Ingredient.objects.create(name=ingredient_list[i % len(ingredient_list)], recipe=recipe)
            Ingredient.objects.create(name='salt', recipe=recipe)

    def tearDown(self) -> None:
        Recipe.objects.all().delete()
        Ingredient.objects.all().delete()

    def test_get_recipes_by_ingredient_name(self):
        test_cases = {
            'bacon': 2,
            'salt': 6,
            'lemon': 0
        }
        for ingredient, count in test_cases.items():
            response = self.query(self.query_string, variables={'name': ingredient})
            self.assertResponseNoErrors(response)
            content = json.loads(response.content)
            self.assertEqual(len(content['data']['searchByIngredient']), count)

    def test_get_recipes_by_ingredient_name_in_title(self):
        recipe = self.recipes[1]
        recipe.title = 'break with bacon'
        recipe.save()

        response = self.query(self.query_string, variables={'name': 'bacon'})
        self.assertResponseNoErrors(response)
        content = json.loads(response.content)
        self.assertEqual(len(content['data']['searchByIngredient']), 3)


class RecipeIngredientCount(GraphQLTestCase):

    def setUp(self):
        self.query_string = '''
            query searchByIngredientCount($count:Int!){
              searchByIngredientCount(count:$count){
                title
                ingredients{
                  name
                }
              }
            }
            '''
        self.recipes = RecipeFactory.create_batch(10)
        for i, recipe in enumerate(self.recipes):
            IngredientFactory.create_batch(i+1, recipe=recipe)

    def tearDown(self) -> None:
        Recipe.objects.all().delete()

    def test_get_recipes_by_ingredient_count(self):

        response = self.query(self.query_string, variables={'count': 5})
        self.assertResponseNoErrors(response)
        content = json.loads(response.content)
        self.assertEqual(len(content['data']['searchByIngredientCount']), 4)
