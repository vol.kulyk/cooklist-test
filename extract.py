
from sqlite3 import IntegrityError
import pandas as pd
import re
from django.db import transaction

import django
django.setup()

from recipes.models import Recipe, Ingredient


r = re.compile(r"[\(\)]|^-?\d+([/\.]\d)?(-\d+)?|\s?\d+([/\.]\d)?(-\d+)?\s(ounce)?|^-|diced|cups?|teaspoons?|"
               r"tablespoons?|tbsp\.?|tsp\.?|lbs?\.?|oz\.?|\sc\.?\s|soft|small|of|slivered|freshly|ground|cloves?|"
               r"chopped|grated|to taste|peeled|seeded|cubed|cooked|roasted|minced|rounds|deveined|trimmed|"
               r"thinly|shredded|undrained|drained|"
               r"melted|large|cut into|thick|strips|wedges|cold|sliced| into|divided|pound|can")


def normalize(text):
    """
    It removes a portion value, its units, stop words
    Ideally it might be used spacy for lemmatization and cleaning from stop words
    regex is used in this function just to not moke this solution overwhelmed
    """
    s = text.replace(' and ', ',')
    s = r.sub('', s)
    return [a.strip() for a in s.split(',') if a.strip()]


def main():

    df = pd.read_csv('cooklist_100_sample_recipes.csv')
    for i, cell in enumerate(df['recipe_data']):
        data = cell.strip().replace('\n\n', '\n').split('\n')

        title = data[0]
        ingredients = []
        first_ingredients_index = 2
        for slice_index, line in enumerate(data[first_ingredients_index:]):
            lower_line = line.lower().strip()
            if 'instructions' in lower_line or 'directions' in lower_line:
                break
            if lower_line:
                normalized = normalize(lower_line)
                ingredients.extend(normalized)
        try:
            with transaction.atomic():
                recipe = Recipe.objects.create(title=title, description=cell)
                ingredients_list = [Ingredient(name=x, recipe=recipe) for x in ingredients]
                Ingredient.objects.bulk_create(ingredients_list)
        except IntegrityError:
            raise


if __name__ == '__main__':
    main()
